from sys import argv
from math import pi, sqrt

###Human genome data
CHR_D = {
                        1 :249250621,
                        2 :243199373,
                        3 :198022430,
                        4 :191154276,
                        5 :180915260,
                        6 :171115067,
                        7 :159138663,
                        8 :146364022,
                        9 :141213431,
                        10:135534747,
                        11:135006516,
                        12:133851895,
                        13:115169878,
                        14:107349540,
                        15:102531392,
                        16:90354753,
                        17:81195210,
                        18:78077248,
                        19:59128983,
                        20:63025520,
                        21:48129895,
                        22:51304566,
                        23:155270560,
}

def chr_size(argv):
	
	CHR_ID = int(argv[1])

	Lg = 3234830000
	d = 6

	CHR_D[0] = Lg 
	
	Ls = CHR_D[CHR_ID]

	###molecule size taken from other manuscript (quoted in the paper)
	V1 = ((Ls+0.0)/Lg)*(4.0/3)*pi*pow((d+0.0)/2,3)
	PHI = pow(V1,1.0/3)
	PHI2 = PHI*sqrt(3)
	
	print V1, PHI, PHI2, '[um3, nm, nm]'

	return V1, PHI, PHI2

if __name__ == '__main__':
	from sys import argv
	
	if len(argv) == 1:
		print "usage: chr_size chromosome_ID"
		print "       nucleus: chromosome_ID=0"
		from sys import exit
		exit(1)

	chr_size(argv)

