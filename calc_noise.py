from sys import argv
from string import split, strip, replace
from numpy import zeros
from scipy.stats import kstest
from math import sqrt, log

def main(argv):
 
 fn = argv[1]
 f = open(fn,'r')
 X = []

 if len(argv) > 2 and argv[2] == '--FAST':
	FAST = True
 else:
	FAST = False

 print "reading 2D file..."
 row = f.readline()

 N = None
 try:
	N = int(strip(row))
	row = f.readline()
 except ValueError:
	pass

 ####heatmap readout
 DIAG2 = True
 i = 0
 FV = 0
 while row != '':
	data = split(strip(row))
	if len(data) == 1:
		data = split(strip(row),'\t')
	if i == 0:
		try:
			V = round(float(data[0]))
		except ValueError:
			row = f.readline()
			data = split(strip(row))
			if len(data) == 1:
				data = split(strip(row),'\t')
			try:
				V = round(float(data[0]))
			except ValueError:
				FV = 1
	for j in range(FV,len(data)):
		v = data[j]
		V = round(float(v),5)
		if V > 0.0 and i < j:
			X.append((V,V*V))
		if abs(i-j) == 1 and V == 0.0:
			DIAG2 = False
	row = f.readline()
	i = i + 1
 f.close()

 if N == None:
	N = i

 print "done.\nsorting..."

 X.sort()
 M = len(X)
 VAL = zeros(M,'f')
 VSQ = zeros(M,'f')
 
 for k in range(M):
 	V, V2 = X[k]
 	VAL[k] = V
 	VSQ[k] = V2
 
 print "done.\nEntering main"

 PV_old = None
 D_old = None
 EX = None
 LOCMIN = 0

 GLOB_DMIN = None
 GLOB_EX = None
 GLOB_STD = None

 if FAST:
	delta = N
 else:
	delta = 1
 k = M

 OPT = []

 ###performing Kolmorogov-Smirnov test
 while k > 1:
 	EX = sum(VAL[:k])/k
 	EX2 = sum(VSQ[:k])/k
	STDEV = sqrt(EX2-EX*EX)
 
 	DATA = (VAL[:k]-EX)/STDEV
 	D, Pv =  kstest(DATA,'norm')
 
 	print D, Pv, k, EX, STDEV

	OPT.append((D,EX,STDEV))

 	k = k - delta
 	PV_old = Pv
	D_old = D
 	EX_OLD = EX
 	STD_OLD = STDEV
	
	if GLOB_DMIN == None or D < GLOB_DMIN:
		GLOB_DMIN = D
		GLOB_PV = Pv
		GLOB_EX = EX
		GLOB_STD = STDEV

 OPT.sort()
 print OPT[0][1], OPT[0][2]

 GLOB_EX, GLOB_STD = OPT[0][1], OPT[0][2]

 return GLOB_EX, GLOB_STD

if __name__ == '__main__':
	from sys import argv

	if len(argv) == 1:
		print "usage: calc_noise heatmap [--FAST]"	
		from sys import exit
		exit(1)

	main(argv)

