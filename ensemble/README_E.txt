============================================================
= GDcast_E script for ensemble generation by MDS algorithm =
============================================================

1. About.

The script was created in order to produce an ensemble of 3D models for chromatin by using Multidimensional Scaling algorithm in (nondeterministic) multistart mode.  The models are then encoded as NMR-like PDB format and the adequate PyMol script is generated to visualize them.

2. Running.

The script is run in the same way as the core GDcast script

3. Parameters.

Please alter the __MODELS__ constant (default: 10) to obtain other nuber of models in the ensemble

4. Installation.

a) the script requires basically the same packages as original GDcast to work

b) the only additional package required is scikit-learn (see also: INSTALL file)

c) after installation, additional works over the package are needed, posibly as a super-user

(i) in sklearn core folder, please navigate to manifold directory

(ii) please copy mds.py as mds_pop.py

(iii) please enter the changes in mds_pop module (smacof function) as specified by the diff file below:

--------------------------

241a242,247
>  
>     ###GD+MDS ensemble extension
>     positions = []
>     stress = None
>     n_iters = None
>     ###
253a260,263
> 
> 	    ###GD+MDS ensemble extension
> 	    positions.append(pos)
> 	    ###
267a278,281
>     ###GD+MDS ensemble extension
>     return positions, stress, n_iters
>     ###
> 

--------------------------

(iv) please edit the __init__ module in the same directory, adding the following lines to at the end of the file

###GD+MDS ensemble extension
from .mds_pop import MDS as MDS_POP
###

