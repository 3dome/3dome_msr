##########################################
### MDS/GD scripts common distribution ###
### v. 1.4                             ###
##########################################

A) Prerequisites:

See: INSTALL file.

B) Scripts:

1) calc_noise

usage: $python calc_noise.py <heatmap>
usage: $python calc_noise.py <heatmap> --FAST

Heatmap has to be in a csv format (space as delimiter) with the first row containing matrix size.

Outputs: expected value, standard deviation as optimum from Kolmogorov-Smirnov test.

Please use --FAST option to enable heuristics which speeds up calculations (~50x) i.e. for large matrices.

2) normalize

usage: $python normalize.py <heatmap>

Heatmap has to be in a csv format (space as delimiter) with the first row containing matrix size.

Output file: a normalized heatmap (for Human nucleus data, level2), *.norm.txt file

C) The main program

usage: $python GDcast.py <heatmap> <miu> <stdev> <n> <norm> <chrID> <stress>
usage: $python GDcast.py --CFG=<config_file>

A configuration file can be used as a substutute for script command line parameters.

The threshold is used for discarding small values below it, calculated by the formula: miu+n*stdev

Norm operator options: std, prob, luk, ham; deprecated: max, nil

MDS Stress functions implemented: sgnrel, kruskal, sammon, sgnsam

Output files:

*.fm.protmap.txt	frequency contact map (before graph distance procedure)
*.pdm.protmap.txt	graph distance map (before MDS procedure)
*.mds.protmap.txt	MDS-generated distance map (after MDS procedure)
*.cm.protmap.txt	auxiliary binary matrix with the notion of positive values on frequency map
*.pdb			PDB model with 3D coordinates
*.a.pdb			mirror image of the PDB model
*.pymol.txt		PyMol script for visualising output PDB files

D) MDS-based ensemble generating script

Please look at the ensemble folder and the appropriate README file

E) Please cite:

Szalaj, P., Tang, Z., Michalski, P., Pietal, M.J., Luo, O.J., Sadowski, M., Li, X., Radew, K.,
 Ruan, Y. and Plewczynski, D., 2016. An integrated 3-Dimensional Genome Modeling Engine for 
 data-driven simulation of spatial genome organization. Genome Research, 26(12), pp.1697-1709.