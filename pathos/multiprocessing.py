class ProcessingPool:

	def __init__(self, nodes):

		self.CORES = nodes

	def map(self, func_name, A0, A1, A2, A3, A4):

		RESULTS = []
		for i in range(len(A0)):
			result = func_name(A0[i],A1[i],A2[i],A3[i],A4[i])
			RESULTS.append(result)

		return RESULTS
