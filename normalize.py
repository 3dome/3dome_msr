from chr_size import CHR_D

#lvl2 =[0, 48, 103, 148, 192, 234, 267, 307, 338, 362, 393, 418, 443, 465, 486, 507, 523, 537, 555, 566, 579, 586, 590, 633]
lvl2 = [0, 99, 207, 300, 389, 469, 541, 602, 658, 702, 759, 804, 858, 892, 918, 951, 976, 1001, 1031, 1052, 1074, 1085, 1101, 1158]

Lg = 3234830000
d = 6
CHR_D[0] = Lg

from math import sqrt, pi
from sys import argv
from string import split, strip

if len(argv) == 1:
	print "usage: normalize heatmap"
	from sys import exit
	exit(1)

fn = argv[1]
MAP = {}

###heatmap readout
f = open(fn,'r')
N = int(f.readline())
row = f.readline()
i = 0
while row != '':
	data = split(strip(row))
	for j in range(N):
		v = data[j]
		V = float(v)
		MAP[(i,j)] = V
	row = f.readline()
	i = i + 1
f.close()

h = open(fn+'.norm.txt','w')

from math import log

D_MUL = []
for k in range(1,23+1):
	for l in range(1, 23+1):
		D_MUL.append(CHR_D[k]*CHR_D[l])

h.write(str(N)+'\n')

I = 0

for i in range(N):
	line_n2 = ''

	###mapping 633x633 level2 matrix onto chromosomal indices
	while I < 23 and i+1 < lvl2[I+1] and i+1 > lvl2[I]:
		I = I + 1
	J = 0

	for j in range(N):
		V = MAP[(i,j)]
		
		while J < 23 and j+1 < lvl2[J+1] and j+1 > lvl2[J]:
			J = J + 1

		#if N == 23:
		#	I, J = i+1, j+1
		
		###actual normalisation code
		V2 = (0.0+1)/((CHR_D[I]*CHR_D[J]+0.0)/max(D_MUL))
		if I == J:
			V2 = V*V2
		else:
			V2 = V/V2

		line_n2 = line_n2 + str(V2) + ' '

	line_n2 = line_n2[:-1] + '\n'
	h.write(line_n2)
h.close()

