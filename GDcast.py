#!/usr/bin/python2.7
from pathos.multiprocessing import ProcessingPool

import orange, orngMDS, math
import orange as Orange

from chr_size import chr_size
from string import split, strip

import sys
sys.setrecursionlimit(100000)

def read(cfg_file):

 ###function for reading a config file
 ###being an alternative for parameters list
 from string import split, strip

 cfg = open(cfg_file,'r')
 row = cfg.readline()

 #obsolete
 param_P = ''
 param_POW = None
 STEP = None
 param_POW2 = None
 #new
 C_THR = 0
 #default
 norm = 'std'
 STRESS = 'sgnrel'

 while row != '':
        data = split(row)
        if len(data) >= 2 and data[0][0] != '#':
                param, val = data[:2]
                if param == 'param_P':
                        param_P = float(val)
                        print "warning: param_P parameter is obsolete"
                elif param == 'param_POW':
                        param_POW = float(val)
                        print "warning: param_POW parameter is obsolete"
                elif param == 'STEP':
                        STEP = int(val)
                        print "warning: STEP parameter is obsolete"
                elif param == 'SCALE':
                        SCALE = float(val)
                elif param == 'OUT_PATH':
                        OUT_PATH = val
                elif param == 'OUT_FN':
                        OUT_FN = val
                elif param == 'param_POW2':
                        param_POW2 = float(val)
                        print "warning: param_POW2 is obsolete"
                elif param == 'fn':
                        fn = val
                elif param == 'C_THR':
                        #new
                        C_THR = float(val)
                elif param == 'norm':
                        #new
                        norm = val
                elif param == 'STRESS':
                        #new
                        STRESS = val

        row = cfg.readline()

 return param_P, param_POW, param_POW2, STEP, SCALE, OUT_PATH, OUT_FN, fn, norm, STRESS, C_THR

class MyMDS(orngMDS.MDS):

	### custom MDS class for counting main function iterations
	def __init__(self, distances=None, dim=2, **kwargs):
		self.CNT = 0
		orngMDS.MDS.__init__(self, distances, dim, **kwargs)

	def callback(self, arg1, arg2):
		self.CNT = self.CNT + 1
		return True

def prob_tnorm(P,Q):

	###probability norm definition
	if len(P) == 0:
		return 0.0

        return P[-1]*Q[-1] + (1-P[-1]*Q[-1])*prob_tnorm(P[:-1],Q[:-1])

def max_tnorm(P,Q):

	###maximum norm definition
	if len(P) == 0:
		return 0.0

	return max(min(P[-1],Q[-1]),max_tnorm(P[:-1],Q[:-1]))

def nil_tnorm(P,Q):

	###nilpotent norm definition	
	if len(P) == 0:
		return 0.0

	S_2 = nil_tnorm(P[:-1],Q[:-1])
	if P[-1]+Q[-1] > 1:
		S_11 =  min(P[-1],Q[-1])
		if S_11 + S_2 < 1:
			return max(S_11,S_2)
		else:
			return 1
	else:
		return S_2

def luk_tnorm(P, Q):

	###Lukasiewicz norm definition
	if len(P) == 0:
		return 0.0

	return min(max(0,P[-1]+Q[-1]-1) + luk_tnorm(P[:-1],Q[:-1]),1)

def std_norm(P, Q):

	###standard norm (standard arithmetic operators) definition
	if len(P) == 0:
		return 0.0

	return P[-1]*Q[-1] + std_norm(P[:-1],Q[:-1])

def tnorm_ham(P, Q):
	
	###Hamacher norm definiton
	if len(P) == 0:
		return 0.0

	if P[-1] == 0 and Q[-1] == 0:
		M = 0.0
	else:
		M = P[-1]*Q[-1]/(P[-1] + Q[-1] - P[-1]*Q[-1])

	T = tnorm_ham(P[:-1],Q[:-1])

	return (M + T)/(1 + M*T)

def partial_map(X, Y, I, N,norm='prob'):
	
	###function that performs partial matrix multiplication using specified norm
	###this function is used in parallell computations
	VVAL = []
	for J in range(I,N+1):
		i,j = I-1, J-1
		if norm == 'prob':
	        	VAL = prob_tnorm(X[i],Y[j])
		elif norm == 'max':
			VAL = max_tnorm(X[i],Y[j])
		elif norm == 'nil':
			VAL = nil_tnorm(X[i],Y[j])
		elif norm == 'luk':
			VAL = luk_tnorm(X[i],Y[j])
		elif norm == 'std':
			VAL = std_norm(X[i], Y[j])
		elif norm == 'ham':
			VAL = tnorm_ham(X[i], Y[j])
                VVAL.append(VAL)

	return VVAL

def main(argv):

 fn = argv[1]

 ###parameters readout
 if fn[:6] == '--CFG=':
        param_P, param_POW, param_POW2, STEP, SCALE, OUT_PATH, OUT_FN, fn, norm, STRESS, C_THR = read(argv[1][6:])
        if len(OUT_PATH) > 0 and OUT_PATH[-1] == '/':
                OUT_PATH = OUT_PATH[:-1]
        OUT_URI = OUT_PATH+'/'+OUT_FN
        if len(OUT_URI) == 1:
                OUT_URI = None
        n = 0
 else:

        OUT_PATH = ''
        OUT_FN = ''
        OUT_URI = None

 	param_P = ''#0.5

	miu = float(argv[2])
 	sigma = float(argv[3])
 	n = float(argv[4])
 	C_THR = miu + n*sigma
	
 	print C_THR#,
	
 	norm = argv[5]
 	SCALE = float(argv[6])
 	STRESS = argv[7]

 from string import split, strip

 if STRESS not in ['sgnrel','kruskal','sammon','sgnsam']:
	raise KeyError, STRESS

 if norm not in ['prob','max','nil','luk','ham', 'std']:
	raise KeyError, norm

 from numpy import empty, zeros, putmask, dot, maximum, logical_xor, minimum, identity, transpose, sum as np_sum
 from math import log, exp, sqrt
 from string import split, strip, find, rfind
 from os import system
 
 from numpy import float64 as Float64
 TYPE = Float64

 print "reading 2D file...",
 
 ###heatmap readout
 rowdata = []
 f = open(fn,'r')
 f.readline()
 row = f.readline()
 while row != '':
 	rowdata.append(strip(row))
 	row = f.readline()
 f.close()
 
 print "done."
 
 N = len(rowdata)
 print N,
 
 Min = 9999 
 Max = -1
 DMin = 9999
 DMax = -1
 Maxminus = -1

 ChrCnt = empty((N,N),'f')
 FORWARD = 0

 DIAG2 = True

 for i in range(N):
 	row = rowdata[i]
 	record = split(row,' ')
 	if len(record) == 1:
 		record = split(row,'\t')

		if len(record) == 1:
			record = split(row,',')

 	for j in range(N):
		try:
 			FF = float(record[j+FORWARD])
		except:
			FORWARD = FORWARD + 1
			FF = float(record[j+FORWARD])

		if j == i + 1 and FF == 0.0:
			DIAG2 = False
	
		if FF > C_THR:
			pass
		else:
			FF = 0.0
			
 		ChrCnt[i][j] = FF

 		if i != j:
 			if FF > Max:
 				Max = FF
 			if abs(i-j) > 1 and FF > 0 and FF < Min:
 				Min = FF
 
 print Min, Max,
 
 ChrP = zeros((N,N),'f')

 if DIAG2 == True:
	DIAG = 0
 else:
	DIAG = 1
 print DIAG2,

 ###2D map fixing, i.e. (i,i+1) values
 for i in range(N):
	for j in range(DIAG+1):
		if i + j < N:
			ChrP[i][i+j] = 1
			ChrP[j+i][i] = 1
 ChrCM = zeros((N,N),'f') 

 ###creating frequencies out of counts
 for i in range(N):
	for j in range(N):
		if j > i + DIAG:
			ChrP[i][j] = round(ChrCnt[i][j]/Max,5)
			ChrP[j][i] = ChrP[i][j]
		if j > i and ChrP[i][j] > 0.0:
			ChrCM[i][j] = 1
			ChrCM[j][i] = 1

 ###further 2D map fixing
 CONV = True
 for i in range(N-1):
        if ChrP[i][i+1] <= 0.5:
                CONV = False
                break
 print CONV
 if not CONV:
	for i in range(N-1):
		ChrP[i][i+1] = 1
		ChrP[i+1][i] = 1
	
 ###auxiliary counters
 SC = 0.0
 SC2 = 0.0
 for i in range(N):
 	SC = SC + np_sum(ChrP[i])
 	for j in range(i+1,N):
 		if ChrP[i][j] > 0.5:
 			SC2 = SC2 + 2
 SC2 = SC2 + N
 SC = SC + N
 if DIAG2 == False:
	SC2 = SC2 + 2*(N-1)
	SC = SC + 2*(N-1)
 print SC, SC2
 
 ID = identity(N,TYPE)
 IDf = identity(N,'f')

 GDM = zeros((N,N),'f')+IDf
 GDM_wrk = zeros((N,N),TYPE)+ID
 diff = zeros((N,N),TYPE)+ID

 ZERO = zeros((N,N),'f')
 ONE = ZERO+1
 
 ZEROS = True
 k = 0
 ZEROSUM = N*N - N

 del rowdata
 del IDf
 del ChrCnt

 ###########################################################################################
 #please alter this value if You install pathos and want to enable parallell GD calculation#
 __CORES__ = 1 #############################################################################
 ###########################################################################################
 
 ###initializing parallell calculations, if applicable
 print "entering main..., cores=", __CORES__
 pool = ProcessingPool(nodes=__CORES__)

 PAD = len(str(ZEROSUM+N))
 print ' '+str(k), '(', str(ZEROSUM+N), ')'

 ###main graph distance loop
 while ZEROSUM > 0:
 	k = k + 1
 	print (2-len(str(k)))*' '+str(k),
	GDTMP = empty((N,N),TYPE)
	i = 0

	while i < N:
		if True:
			l = 0
			AA = {0:[],1:[],2:[],3:[],4:[]}
			while l < __CORES__ and i+l < N:
				AA[0].append(GDM_wrk)
				AA[1].append(ChrP)
				AA[2].append(i+1+l)
				AA[3].append(N)
				AA[4].append(norm)
				l = l + 1
			result = pool.map(partial_map, AA[0], AA[1], AA[2], AA[3], AA[4])
			for m in range(l):
				VARR = result[m]
				R1 = AA[2][m]
				R2 = N
				for j in range(R1,R2+1):
					VAL = VARR[j-R1]
					GDTMP[i+m][j-1] = VAL
					GDTMP[j-1][i+m] = VAL
					
				GDTMP[i+m][i+m] = 1.0
		i = i + l

	GDM_wrk = GDTMP

        new_diff = zeros((N,N),TYPE)
	putmask(new_diff,maximum(GDM_wrk-0.5,ZERO), ONE)
 	
 	new_diff = new_diff - diff
 	
	curr = zeros((N,N),'f')
 	putmask(curr,new_diff,ONE*k)
 	GDM = GDM + curr

	ZEROSUM = ZEROSUM - np_sum(new_diff,dtype=TYPE)
	ZEROSUM = int(ZEROSUM)
	PAD2 = PAD - len(str(ZEROSUM))
	print '(', PAD2*' '+str(ZEROSUM), ')'

 	diff = diff + new_diff

 K = k
 print "done."
 
 ###graph distance and frequency encoding to protmap2d format
 g = open(fn+'_'+str(param_P)+'_'+str(n)+'.cm.protmap.txt','w')
 h = open(fn+'_'+str(param_P)+'_'+str(n)+'.fm.protmap.txt','w')
 gdm = open(fn+'_'+str(param_P)+'_'+str(n)+'.pdm.protmap.txt','w')
 
 g.write('PFRMAT ChrCM\n')
 h.write('PFRMAT ChrFCM\n')
 gdm.write('PFRMAT ChrGDM\n')
 
 k = 0
 while (k+1)*50 < N:
        g.write('SEQRES  '+50*'A'+'\n')
        h.write('SEQRES  '+50*'A'+'\n')
        gdm.write('SEQRES  '+50*'A'+'\n')
        k = k + 1
 g.write('SEQRES  '+(N-k*50)*'A'+'\n')
 h.write('SEQRES  '+(N-k*50)*'A'+'\n')
 gdm.write('SEQRES  '+(N-k*50)*'A'+'\n')
 
 g.write('MODEL 0\n')
 h.write('MODEL 0\n')
 gdm.write('MODEL 0\n')
 
 for I in range(N):
 	for J in range(I+1,N):
		i,j = I+1,J+1
		p = round(ChrCM[I][J],3)
		if p > 0.0:
 			line = (4-len(str(i)))*' '+str(i)+' '+(4-len(str(j)))*' '+str(j)+' -1 -1 '+str(p) + '\n'
 			g.write(line)
 		q = round(ChrP[I][J],3)
 		if q > 0.0:
 			line = (4-len(str(i)))*' '+str(i)+' '+(4-len(str(j)))*' '+str(j)+' -1 -1 '+str(q) + '\n'
 			h.write(line)
 		s = round(GDM[I][J],3)
                if s > 0.0:
                        line = (4-len(str(i)))*' '+str(i)+' '+(4-len(str(j)))*' '+str(j)+' -1 -1 '+str(s) + '\n'
                        gdm.write(line)
 
 g.write('END\n')
 h.write('END\n')
 gdm.write('END\n')
 
 g.close()
 h.close()
 gdm.close()

 ###scale introduction based on chromosome ID
 CHR_ID = int(SCALE)

 V, P, SCALE_MAX = chr_size(['',CHR_ID])
 print SCALE_MAX, 
 SCALE = SCALE_MAX/K
 print SCALE

 ###MDS part
 matrix = orange.SymMatrix(N)
 for i in range(N):
    for j in range(N):
        matrix[i, j] = SCALE*GDM[i][j]
 
 mds = MyMDS(matrix, 3)
 print "running MDS..."
 
 if STRESS == 'sgnrel':
	sf = orngMDS.SgnRelStress
 elif STRESS == 'kruskal':
	sf = orngMDS.KruskalStress
 elif STRESS == 'sammon':
	sf = orngMDS.SammonStress
 elif STRESS == 'sgnsam':
	sf = orngMDS.SgnSammonStress

 mds.run(100000,stress_func=sf,eps=1e-12,progress_callback=mds.callback)
 print "done in", mds.CNT, "steps."
 
 ###pdb files' encoding after mds
 if OUT_URI == None:
	 OUT_URI = fn+'_'+str(param_P)+'_'+str(n)+'.pdb'
 pdb = open(OUT_URI,'w')
 pdb_A = open(OUT_URI[:-4]+'.a.pdb','w')
 
 i = 1
 for p in mds.points:
 	x, y, z = p
 	X, Y, Z = round(x,3), round(y,3), round(-z,3)
	Z_A = -Z
 	if X > 0:
 		X = ' '+str(X)
 	else:
 		X = str(X)
 	if len(X) < 7:
 		X = X+(7-len(X))*' '
        if Y > 0:
                Y = ' '+str(Y)
        else:
                Y = str(Y)
        if len(Y) < 7:
                Y = Y+(7-len(Y))*' '
        if Z > 0:
                Z = ' '+str(Z)
        else:
                Z = str(Z)
        if len(Z) < 7:
                Z = Z+(7-len(Z))*' '
	if Z_A > 0:
		Z_A = ' '+str(Z_A)
	else:
		Z_A = str(Z_A)
	if len(Z_A) < 7:
		Z_A = Z_A + (7-len(Z_A))*' '
 
 	line = 'ATOM    '+(3-len(str(i)))*' '+str(i)+'  CA  ALA A '+(3-len(str(i)))*' '+str(i)+'     '
 	line2 = X+' '+Y+' '+Z+' 1.00 99.99\n'
	line3 = X+' '+Y+' '+Z_A+' 1.00 99.99\n'
 	pdb.write(line+line2)
	pdb_A.write(line+line3)
 	i = i + 1
 pdb.close()
 pdb_A.close()

 Z = find(fn,'lvl')
 if Z >= 0:
	LVL = fn[Z+3]
 else:
	LVL = '9'
 RNAME = 'L'+LVL+'_'+str(n)+'_'
 
 ###pymol script connecting and coloring dotted pdb pseudoatoms encoding
 xx = open(fn+'_'+str(param_P)+'_'+str(n)+'.pymol.txt','w')
 xx.write('set connect_mode, 1\n')
 xx.write('set stick_radius, 0.1\n')
 xx.write('set sphere_scale, 0.25\n')
 for MODE in ['','.a']:
	 RNAME = RNAME + MODE
	 pdb_file = fn+'_'+str(param_P)+'_'+str(n)+MODE+'.pdb'
	 xx.write('load '+pdb_file+'\n')
	 for i in range(N):
 		I = str(i+1)
	 	xx.write('select '+RNAME+I+', resi '+I+'\n')
 
	 for i in range(N-1):
 		I = str(i+1)
	 	J = str(i+2)
 		xx.write('bond '+RNAME+I+', '+RNAME+J+'\n')
	
	 xx.write('select '+pdb_file[rfind(fn,'/')+1:-4]+'\n')
	 xx.write('spectrum count, red_blue, sele\n')
	 xx.write('set fog, 0\n')
	 xx.write('set_bond stick_radius, 0.1, sele\n')
	 xx.write('show sticks\n')
	 for i in range(N):
		I = str(i+1)
		xx.write('delete '+RNAME+I+'\n')
	 xx.write('orient\n')
	 xx.write('deselect\n')
 xx.write('delete sele\n')
 xx.close()
 
 ###mds 3D approximate euclidean map encoding to protmap2d format
 mdsmap = open(fn+'_'+str(param_P)+'_'+str(n)+'.mds.protmap.txt','w')
 mdsmap.write('PFRMAT ChrMDS\n')
 k = 0

 while (k+1)*50 < N:
         mdsmap.write('SEQRES  '+50*'A'+'\n')
         k = k + 1

 mdsmap.write('SEQRES  '+(N-k*50)*'A'+'\n')

 mdsmap.write('MODEL 0\n')
 
 Dmax = None
 EX2 = 0.0
 
 Dneigh = 0.0
 for i in range(N):
 	for j in range(i+1,N):
 		x1,y1,z1 = mds.points[i]
 		x2,y2,z2 = mds.points[j]
 		
 		d = sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)+(z1-z2)*(z1-z2))
 
 		if j == i + 1:
 			Dneigh = Dneigh + d
 			EX2 = EX2 + d*d
 
 		if Dmax == None or d > Dmax:
 			Dmax = d
 		
                I,J = i+1,j+1
                line = (4-len(str(I)))*' '+str(I)+' '+(4-len(str(J)))*' '+str(J)+' -1 -1 '+str(d) + '\n'
                mdsmap.write(line)
 mdsmap.write('END\n')
 mdsmap.close()
 
 EX2 = EX2/(N-1)
 Dneigh = Dneigh/(N-1)
 STDEV = sqrt(EX2 - Dneigh*Dneigh)

 return Dmax, K, SC2/N, SC/N, Dneigh, STDEV, SCALE

if __name__ == '__main__':
	from sys import argv
	if len(argv) < 8 and not ( len(argv) == 2 and argv[1][:6] == '--CFG=') :
		print "usage: GDcast heatmap miu stdev n norm chr_id stress"
		print "usage: GDcast --CFG=config_file"
		from sys import exit
		exit(1)

	results = main(argv)

